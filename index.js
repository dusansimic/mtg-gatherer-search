const got = require('got');
const $ = require('cheerio');

/**
 * Get mana object
 * @param {Object} doc Part of document that contains mana list
 * @returns {Array} Mana object
 */
function getMana(doc) {
	// Parse that mana html
	const manaList = doc.map(obj => obj.hasOwnProperty('attribs') ? obj.attribs.alt : undefined);

	const mana = { 'Any': 0, 'Red': 0, 'White': 0, 'Green': 0, 'Blue': 0, 'Black': 0, 'Artifact': 0, 'Total': 0 };
	manaList.map(val => {
		if (!isNaN(val)) {
			mana['Any'] += parseInt(val, 10);
			mana['Total'] += mana['Any'];
		} else if (val != undefined) {
			mana[val]++;
			mana['Total']++;
		}
	});

	return mana;
}

/**
 * MTG Gatherer Scraper
 * @param {String} cardName Name of the card you search for
 */
const mgs = async cardName => {
	// If card name is not specified, scream
	const hasCardName = Boolean(cardName);
	if (!hasCardName) {
		throw new Error('Card name not specified!');
	}

	let cardNameUrl = '';
	for (const name of cardName.split(' ')) {
		cardNameUrl += `+[${name}]`;
	}

	// Get Magic Gatherer search page html
	const response = await got.get(`http://gatherer.wizards.com/Pages/Search/Default.aspx?name=${cardNameUrl}`);
	const document = response.body;

	// Query table from html
	const table = $('.cardItemTable tbody tr td table tbody .cardItem.evenItem', document);
	if (table.length == 0) {
		const title = $('#ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_nameRow .value', document)[0].children[0].data.trim();
		// console.log($('.row.manaRow .value', document)[0].children);
		const mana = getMana($('.row.manaRow .value', document)[0].children);
		// Get the image url
		const imageUrlRelative = $('.cardDetails tbody tr .cardImage img', document)[0].attribs.src
		const imageUrl = `http://gatherer.wizards.com/${imageUrlRelative.slice(6)}`;
		// Get id out of image url
		const multiverseId = parseInt(/multiverseid\s*=\s*(.*)&type/.exec(imageUrlRelative)[1]);

		return [{
			title,
			mana,
			imageUrl,
			multiverseId
		}];
	}
	// Get middle and left col from table
	const middleCol = $('.middleCol .cardInfo', table)
	const leftCol = $('.leftCol', table);

	// Get titles of cards
	const titleDocument = $('.cardTitle a', middleCol);

	const data = [];
	for (let i = 0; i < table.length; i++) {
		const mana = getMana(middleCol[i]);

		// Get the image url
		const imageUrlRelative = $('a img', leftCol[i])[0].attribs.src;
		const imageUrl = `http://gatherer.wizards.com/${imageUrlRelative.slice(6)}`;

		// Get id out of image url
		const multiverseId = parseInt(/multiverseid\s*=\s*(.*)&type/.exec(imageUrlRelative)[1]);

		// Create an object
		const object = {
			title: titleDocument[i].children[0].data,
			mana,
			imageUrl,
			multiverseId
		};
		data.push(object);
	}

	return data;
};

module.exports = mgs;
